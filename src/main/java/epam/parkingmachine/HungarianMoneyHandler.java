package epam.parkingmachine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class HungarianMoneyHandler implements MoneyHandler {

	enum moneyType {
		FIVE(5), TEN(10), TWENTY(20), FIFTY(50), HUNDRED(100), INVALID(0);

		double value;

		moneyType(double value) {
			this.value = value;
		}

		static moneyType getTypeByInt(double num) {
			for (moneyType money : moneyType.values()) {
				if (num == money.value) {
					return money;
				}
			}
			return moneyType.INVALID;
		}

	};

	private List<moneyType> insertedMoney;
	private final String currency = "HUF";

	public HungarianMoneyHandler() {
		insertedMoney = new ArrayList<>();
	}

	public double getInsertedMoneyValue() {
		double sum = 0;
		for (moneyType money : insertedMoney) {
			sum += money.value;
		}
		return sum;

	}

	public void giveBack(double moneyBack) {
		System.out.println("given back " + moneyBack);
	}

	public String getCurrency() {
		return currency;
	}
 
	public void waitForMoney() {
		System.out.println("To finish payment type: \"pay\" ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String line = br.readLine();
			while (line != null && !line.equals("pay")) {
				double num = Double.parseDouble(line);
				addToList(num);
				line = br.readLine();

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addToList(double num) {
		moneyType money = moneyType.getTypeByInt(num);
		if (money != moneyType.INVALID) {
			insertedMoney.add(money);
		} else {
			System.out.println("Invalid money type ");
		}
	}

}
