package epam.parkingmachine;

import epam.ticketstate.TicketState;
import epam.ticketstate.UnPaidTicket;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		if (args.length == 1) {
			Ticket ticket = new Ticket(Integer.parseInt(args[0]));
			PriceGenerator generator = new HungarianPriceGenerator();
			double price = generator.generatePrice(ticket.getId());
			ticket.setPrice(price);
			ticket.setState(new UnPaidTicket());
			MoneyHandler moneyHandler = new HungarianMoneyHandler();
			System.out.println("Please pay " + price + " " + moneyHandler.getCurrency());
			
			moneyHandler.waitForMoney();

			PaymentHandler payment = new HungarianPaymentHandler(moneyHandler);
			payment.pay(ticket);
		} else {
			System.out.println("CSAK EGY VAN!!!!!");
		}

		// for(String id : args){
		// Ticket ticket = new Ticket(Integer.parseInt(id));
		//
		// }
	}
}
