package epam.parkingmachine;

public interface PriceGenerator {
	
	double generatePrice(int id);

}
