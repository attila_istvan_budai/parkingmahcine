package epam.parkingmachine;

import epam.ticketstate.NoPriceTicket;
import epam.ticketstate.TicketState;

public class Ticket {
	
	private int id;
	private double price;
	
	private TicketState myState;
	
	
	public Ticket(int id)
	{
		this.id = id;
		myState = new NoPriceTicket();
	}
	
	public int getId() {
		return id;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
    TicketState getState() {
        return myState;
    }
	
    public void setState(final TicketState newState) {
        myState = newState;
    }
    
    public void payTicket(MoneyHandler moneyHandler){
    	myState.payTicket(this, moneyHandler);
    }

	
	

}
