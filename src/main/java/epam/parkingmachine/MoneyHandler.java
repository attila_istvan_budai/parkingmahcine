package epam.parkingmachine;

public interface MoneyHandler {
	
	double getInsertedMoneyValue();

	void giveBack(double moneyBack);
	
	String getCurrency();
	
	public void waitForMoney();

}
