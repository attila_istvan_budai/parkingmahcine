package epam.parkingmachine;

public interface PaymentHandler {
	
	void pay(Ticket ticket);

}
