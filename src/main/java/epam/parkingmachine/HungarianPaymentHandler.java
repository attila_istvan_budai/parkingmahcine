package epam.parkingmachine;

import epam.ticketstate.TicketState;

public class HungarianPaymentHandler implements PaymentHandler {
	
	MoneyHandler moneyHandler;
	
	public HungarianPaymentHandler(MoneyHandler coinHandler)
	{
		this.moneyHandler = coinHandler;
	}

	public void pay(Ticket ticket) {
		ticket.payTicket(moneyHandler);
	}

}
