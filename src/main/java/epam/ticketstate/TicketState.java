package epam.ticketstate;

import epam.parkingmachine.MoneyHandler;
import epam.parkingmachine.Ticket;

public interface TicketState {
	
	void payTicket(Ticket ticket, MoneyHandler coinHandler);

}
