package epam.ticketstate;

import epam.parkingmachine.MoneyHandler;
import epam.parkingmachine.Ticket;

public class PaidTicket implements TicketState {

	@Override
	public void payTicket(Ticket ticket, MoneyHandler coinHandler){
		System.out.println("Ticket has been payed already");

	}

}
