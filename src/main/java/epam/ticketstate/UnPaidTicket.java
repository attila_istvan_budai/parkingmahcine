package epam.ticketstate;

import epam.parkingmachine.MoneyHandler;
import epam.parkingmachine.Ticket;

public class UnPaidTicket implements TicketState {

	@Override
	public void payTicket(Ticket ticket, MoneyHandler moneyHandler) {
		double price = ticket.getPrice();
		if (price < 0) {
			System.out.println("Price can't be less than 0");
		} else if (price == 0) {
			System.out.println("Free of charge");
			ticket.setState(new PaidTicket());
		} else {
			double insertedCoinValue = moneyHandler.getInsertedMoneyValue();
			if (insertedCoinValue >= price) {
				ticket.setState(new PaidTicket());
				double moneyReturn = insertedCoinValue - price;
				moneyHandler.giveBack(moneyReturn);
			} else {
				System.out.println("Give me more money hahahahhaa");
			}
		}
	}

}
