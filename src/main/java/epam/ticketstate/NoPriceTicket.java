package epam.ticketstate;

import epam.parkingmachine.MoneyHandler;
import epam.parkingmachine.Ticket;

public class NoPriceTicket implements TicketState {

	@Override
	public void payTicket(Ticket ticket, MoneyHandler coinHandler){
		System.out.println("No price has been set");
	}

}
