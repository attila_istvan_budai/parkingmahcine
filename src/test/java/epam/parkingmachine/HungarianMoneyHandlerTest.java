package epam.parkingmachine;

import junit.framework.Assert;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import epam.parkingmachine.HungarianMoneyHandler.moneyType;

public class HungarianMoneyHandlerTest {
	
	@Test
	public void getInsertedMoneyValueWhenThreeValueAddedShouldReturnTrue()	{
		HungarianMoneyHandler handler = new HungarianMoneyHandler();
		handler.addToList(50);
		handler.addToList(20);
		handler.addToList(10);
		double actual = handler.getInsertedMoneyValue();
		Assert.assertEquals(80.0, actual);
	}
	
	@Test
	public void getInsertedMoneyValueWhenThreeValueAddedOneIsInvalidShouldReturnTwoValueSum()	{
		HungarianMoneyHandler handler = new HungarianMoneyHandler();
		handler.addToList(50);
		handler.addToList(20);
		handler.addToList(11);
		double actual = handler.getInsertedMoneyValue();
		Assert.assertEquals(70.0, actual);
	}
	
	@Test
	public void testInvalidMoneyTypeWhenNumberIsValidShouldReturnValidNumber(){
		moneyType money = moneyType.getTypeByInt(10);
		Assert.assertEquals(moneyType.TEN, money);
	}
	
	@Test
	public void testInvalidMoneyTypeWhenNumberIsInvalidShouldReturnInvalidNumber(){
		moneyType money = moneyType.getTypeByInt(11);
		Assert.assertEquals(moneyType.INVALID, money);
	}
	
	@Test
	public void getCurrencyTest(){
		HungarianMoneyHandler handler = new HungarianMoneyHandler();
		String currency = handler.getCurrency();
		Assert.assertEquals("HUF", currency);
	}
	

}
